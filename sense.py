from sense_hat import SenseHat
import sys
from datetime import datetime
import time

import requests
import json

assert ('linux' in sys.platform), "This code runs on Linux only."


sense = SenseHat()
sense.clear()

def start_sequence_gen():
    n = 3
    print('Script will starts, in: ')
    yield n
    print(n)

    n -= 1
    yield n
    print(n)

    n -= 1
    yield n
    print(n)
    
    n -= 1
    yield n
    print(n)

for countdown in start_sequence_gen():
    print(" starting in"),
    
    

#while True:
# Take readings from all three sensors
t = sense.get_temperature()
p = sense.get_pressure()
h = sense.get_humidity()#

t = round(t, 1)
p = round(p, 0)
h = round(h, 0)
now = datetime.now()

create_time = now.strftime("%Y-%m-%d %H:%M:%S");
message = str(t) + ";" + str(h) + ";" + str(p) + ";" + create_time
print(message)
try:
    file= open("log.csv", "a")
    file.write(message + "\n");
    file.close()
except FileNotFoundError as fnf_error:
    print(fnf_error)
    
API_ENDPOINT = "http://itk.rocks/weather/"

wth ={ 'temperature': t, 'pressure': p, 'humidity': h }
resp = requests.post(API_ENDPOINT, json=wth)
print("Status code: ",resp.status_code)
print("JSON: ", resp.json())
    
#    time.sleep(300)
    
