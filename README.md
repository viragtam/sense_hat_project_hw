# Quick start Guide
This is the Raspberry side repository for the Weather Logger project for Scientific python subject.
---


For use it, you will need the following 10 steps.

1. Get a **Raspberry Pi** 2/3/4 A or B, a **Sense hat** kit, a **USB Power supply** (in our case a 5V@2A Apple iPad2 USB adapter), a 8GB **Micro SD card**, a PC, **UTP cable**.
2. Download the latest [Raspbian, or Raspberry PI OS](https://www.raspberrypi.org/software/), and install the OS to the SD card, following the guide.
3. Enable SSH: Open the SD card Boot directory, and create an empty file named as **ssh**
4. Assemble it, and plug in the USB supply, connect the Raspberry Pi to a DHCP capable Router, modem, HomeGateway, etc via UTP cable. Search for the Pi-s IP address in your Router configuration page.
5. Connect to the **Raspberry Pi** via **ssh**, run the raspi-config
```sh
sudo raspi-config
```
and enable SPI, I2C connections, at interface connections.
6. Install sense-hat libraries
```sh
sudo apt-get install sense-hat 
```
7. Clone the repository to the home directory.
```sh
git clone https://bitbucket.org/viragtam/sense_hat_project_hw.git
```
8. Set periodically run the script with crontab, and set your favorite editor. 
```sh
crontab -e
```
Add to the end of the file the following line, what runs the script in every 5 min from the boot of the Raspberry.
```sh
*/5 * * * *  python /home/<reposotory location>/sense.py
```
9. Reboot your Pi
```sh
sudo reboot
```
10. After 5-6 minutes, check new values at [REST API page](http://itk.rocks/weather/)

---
Enjoy the logger ;)
